package com.infodiegohermoso.myform;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button btnNext;
    private EditText edtName;
    private String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_icon);
        btnNext = findViewById(R.id.btnNextStep);
        edtName = findViewById(R.id.edtName);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = edtName.getText().toString();

                Intent i = new Intent(MainActivity.this,SecondActivity.class);

                i.putExtra("nombre",name);

                startActivity(i);
            }
        });
    }
}
