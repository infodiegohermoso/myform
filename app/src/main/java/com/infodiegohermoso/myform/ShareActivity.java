package com.infodiegohermoso.myform;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class ShareActivity extends AppCompatActivity {
    private Button btnNext;
    private String frase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        btnNext = findViewById(R.id.btnNextStep);
       frase = getIntent().getStringExtra("frase");

        Toast.makeText(this, frase, Toast.LENGTH_SHORT).show();


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share(frase);
            }
        });






    }




    private void share(String frase)
    {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT,frase);
        shareIntent.setType("text/plain");
        startActivity(shareIntent);
    }
}
