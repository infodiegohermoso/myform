package com.infodiegohermoso.myform;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
    private RadioGroup radioGroup;
    private RadioButton radioButton1;
    private  RadioButton radioButton2;
    private SeekBar seekBar;
    private TextView tvNumber;
    private Button btnNext;
    private int edad;
    private boolean frase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        radioGroup = findViewById(R.id.RadioGroup);
        radioButton1 = findViewById(R.id.RadioBtn1);
        radioButton2 = findViewById(R.id.RadioBtn2);
        seekBar = findViewById(R.id.seekBarEdad);
        tvNumber = findViewById(R.id.tvNumber);
        btnNext = findViewById(R.id.btnNextStep);

        // Funcionamiento SeekBar
        seekBar();
        final String name = getIntent().getStringExtra("nombre");
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Comprobamos los radioBtn

                    checkRadioBtn();

                    // Obtenemos la edad, del SeekBar
                      edad = seekBar.getProgress();
                      String edadSelected = String.valueOf(edad);

                    if (frase)
                    {

                        Toast.makeText(SecondActivity.this, String.valueOf(edadSelected), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(SecondActivity.this,ThirdActivity.class);
                        i.putExtra("frase","Hola " + name + " ¿Cómo llevas esos " + edadSelected + " años? #MyForm");
                        startActivity(i);

                    }
                    else
                    {
                        Toast.makeText(SecondActivity.this, String.valueOf(edad + 1), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(SecondActivity.this,ThirdActivity.class);
                        i.putExtra("frase", "Espero vere pronto " + name + " antes que cumplas los " + edad + " #MyForm");
                        startActivity(i);
                    }




            }
        });




    }

    private void checkRadioBtn()
    {
        if (radioButton1.isChecked() || radioButton2.isChecked())
        {
            if (radioButton1.isChecked())
            {
                frase = true;
            }
            else
            {
                frase = false;
            }
        }
        else
        {
            Toast.makeText(SecondActivity.this, "Debe Die todos los campos", Toast.LENGTH_SHORT).show();
        }

    }

    private void seekBar()
    {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvNumber.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }










}



