package com.infodiegohermoso.myform;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class ThirdActivity extends AppCompatActivity {
    private ImageButton imageButton;
    String frase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        imageButton = findViewById(R.id.imageButton);

        frase = getIntent().getStringExtra("frase");

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ThirdActivity.this,ShareActivity.class);
                i.putExtra("frase",frase);
                startActivity(i);


            }
        });
    }
}
